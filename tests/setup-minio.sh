#!/bin/bash
set -euo pipefail

# Setup temporary minio server.

export LOCALMINIO_ENDPOINT=${MINIO_URL:-http://localhost:9000}
export ARTIFACTS_BUCKET="artifacts-bucket"
export ARTIFACTS_PATH="artifacts-path/"
export BUCKET_ARTIFACTS="${LOCALMINIO_ENDPOINT}/${ARTIFACTS_BUCKET}/${ARTIFACTS_PATH}"
export BUCKET_ARTIFACTS_BUCKET_AWS_ACCESS_KEY_ID=user
export BUCKET_ARTIFACTS_BUCKET_AWS_SECRET_ACCESS_KEY=password
declare -p BUCKET_ARTIFACTS BUCKET_ARTIFACTS_BUCKET_AWS_ACCESS_KEY_ID BUCKET_ARTIFACTS_BUCKET_AWS_SECRET_ACCESS_KEY LOCALMINIO_ENDPOINT ARTIFACTS_BUCKET ARTIFACTS_PATH

if ! [ -v MINIO_URL ]; then  # if not set (by GitLab CI), start a MinIO instance here
    if [ ! -x "/tmp/minio" ]; then
        curl --retry 5 -Lso /tmp/minio \
        https://dl.min.io/server/minio/release/linux-amd64/minio
        chmod +x /tmp/minio
    fi
    rm -rf /tmp/minio-root
    mkdir -p /tmp/minio-root
    MINIO_ACCESS_KEY="${BUCKET_ARTIFACTS_BUCKET_AWS_ACCESS_KEY_ID}" MINIO_SECRET_KEY="${BUCKET_ARTIFACTS_BUCKET_AWS_SECRET_ACCESS_KEY}" \
        /tmp/minio server /tmp/minio-root > /dev/null &
    LOCALMINIO_PID=$!
    echo 'export LOCALMINIO_PID="'"${LOCALMINIO_PID}"'"'
    echo "trap 'kill ${LOCALMINIO_PID}' EXIT"
fi

# Setup buckets on temporary minio server if they do not exist yet
AWS_ACCESS_KEY_ID="${BUCKET_ARTIFACTS_BUCKET_AWS_ACCESS_KEY_ID}" AWS_SECRET_ACCESS_KEY="${BUCKET_ARTIFACTS_BUCKET_AWS_SECRET_ACCESS_KEY}" \
    aws --endpoint-url "${LOCALMINIO_ENDPOINT}" s3api create-bucket --bucket "${ARTIFACTS_BUCKET}" > /dev/null
