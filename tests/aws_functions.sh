#!/bin/bash

# disable some warnings as shellcheck is not able to see the dynamically
# imported functions via eval() and has some assumptions about variable use
# that are not valid for unit tests
# shellcheck disable=SC2034,SC2030,SC2031

set -euo pipefail

# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh

_failed_init

# stub aws command and record params and relevant exported env variables
function aws {
    if [ -f /tmp/aws_params.2 ]; then mv /tmp/aws_params.2 /tmp/aws_params.3; fi
    if [ -f /tmp/aws_params ]; then mv /tmp/aws_params /tmp/aws_params.2; fi
    echo "$@" > /tmp/aws_params
    bash -c 'echo ${AWS_ACCESS_KEY_ID}' > /tmp/aws_access_key
    bash -c 'echo ${AWS_SECRET_ACCESS_KEY}' > /tmp/aws_secret_key
}

function _clear_aws {
    rm -f /tmp/aws_params /tmp/aws_access_key /tmp/aws_secret_key
}

function _check_leaked_vars {
    local aws_var_count
    aws_var_count=$(set | grep -c ^AWS_S3_ || true)

    _check_equal "${aws_var_count}" "0" "len(env[AWS_S3_*])" "Did no environment variables leak from the AWS functions"
}

function check_bucket {
    local BUCKET="https://endpoint.url/bucket"
    eval "$(aws_vars BUCKET)"

    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly without a path"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_PATH}" "" "AWS_S3_PATH" "Is the path set correctly without a path"
}
check_bucket

function check_bucket_slash {
    local BUCKET="https://endpoint.url/bucket/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly without a path but a slash"
    _check_equal "${AWS_S3_PATH}" "" "AWS_S3_PATH" "Is the path set correctly without a path but a slash"
}
check_bucket_slash

function check_bucket_slash_path {
    local BUCKET="https://endpoint.url/bucket/path"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly with a path missing a slash"
    _check_equal "${AWS_S3_PATH}" "path/" "AWS_S3_PATH" "Is the path set correctly with a path missing a slash"
}
check_bucket_slash_path

function check_bucket_slash_path_slash {
    local BUCKET="https://endpoint.url/bucket/path/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly with a path with a trailing slash"
    _check_equal "${AWS_S3_PATH}" "path/" "AWS_S3_PATH" "Is the path set correctly with a path with a trailing slash"
}
check_bucket_slash_path_slash

function check_bucket_slash_path_slash_subpath {
    local BUCKET="https://endpoint.url/bucket/path/subpath"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_S3_BUCKET}" "bucket" "AWS_S3_BUCKET" "Is the bucket set correctly with a subpath"
    _check_equal "${AWS_S3_PATH}" "path/subpath/" "AWS_S3_PATH" "Is the path set correctly with a subpath"
}
check_bucket_slash_path_slash_subpath

function check_aws {
    local AWS_ACCESS_KEY_ID_exists AWS_SECRET_ACCESS_KEY_exists BUCKET="https://endpoint.url/bucket/path/"
    eval "$(aws_vars BUCKET)"

    AWS_ACCESS_KEY_ID_exists="$([ -v AWS_ACCESS_KEY_ID ] && echo 1 || echo 0)"
    AWS_SECRET_ACCESS_KEY_exists="$([ -v AWS_SECRET_ACCESS_KEY ] && echo 1 || echo 0)"

    _check_equal "${AWS_ACCESS_KEY_ID_exists}" "0" "-v AWS_ACCESS_KEY_ID" "Is the access key kept undefined if requested"
    _check_equal "${AWS_SECRET_ACCESS_KEY_exists}" "0" "-v AWS_SECRET_ACCESS_KEY" "Is the secret key kept undefined if requested"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_ENDPOINT_PARAMS[*]}" "--endpoint-url https://endpoint.url" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter correct"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "endpoint.url" "AWS_S3_ENDPOINT_HOST" "Is the endpoint host correct"
    # shellcheck disable=SC2154
    _check_equal "${AWS_S3_ENDPOINT_SCHEME}" "https" "AWS_S3_ENDPOINT_SCHEME" "Is the endpoint scheme correct"
}
check_aws

function check_aws_credentials {
    local BUCKET="https://endpoint.url/bucket-name/path/"
    local BUCKET_BUCKET_NAME_AWS_ACCESS_KEY_ID="access-key"
    local BUCKET_BUCKET_NAME_AWS_SECRET_ACCESS_KEY="secret-key"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_ACCESS_KEY_ID}" "access-key" "aws_access_key" "Is the access key correct"
    _check_equal "${AWS_SECRET_ACCESS_KEY}" "secret-key" "aws_secret_key" "Is the secret key correct"
    _check_equal "${AWS_S3_ENDPOINT_PARAMS[*]}" "--endpoint-url https://endpoint.url" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter correct"
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "endpoint.url" "AWS_S3_ENDPOINT_HOST" "Is the endpoint host correct"
    _check_equal "${AWS_S3_ENDPOINT_SCHEME}" "https" "AWS_S3_ENDPOINT_SCHEME" "Is the endpoint scheme correct"
}
check_aws_credentials

function check_aws_credentials_proxy {
    local BUCKET="https://endpoint.url/bucket-name/path/"
    local BUCKET_BUCKET_NAME_AWS_ACCESS_KEY_ID="access-key"
    local BUCKET_BUCKET_NAME_AWS_SECRET_ACCESS_KEY="secret-key"
    local BUCKET_BUCKET_NAME_PROXY="http://proxy.url/"
    eval "$(aws_vars BUCKET)"

    _check_equal "${AWS_ACCESS_KEY_ID}" "access-key" "aws_access_key" "Is the access key correct"
    _check_equal "${AWS_SECRET_ACCESS_KEY}" "secret-key" "aws_secret_key" "Is the secret key correct"
    _check_equal "${AWS_S3_ENDPOINT_PARAMS[*]}" "--endpoint-url https://endpoint.url" "AWS_S3_ENDPOINT_PARAMS" "Is the endpoint parameter correct"
    _check_equal "${AWS_S3_ENDPOINT_HOST}" "proxy.url" "AWS_S3_ENDPOINT_HOST" "Is the proxy endpoint host correct"
    _check_equal "${AWS_S3_ENDPOINT_SCHEME}" "http" "AWS_S3_ENDPOINT_SCHEME" "Is the proxy endpoint scheme correct"
}
check_aws_credentials_proxy

function check_aws_s3_download_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_download BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the download access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the download secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 cp s3://bucket/path/from to" "aws_params" "Is the download command correct for AWS"
}
check_aws_s3_download_command

function check_aws_s3_download_command_credentials {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local BUCKET_BUCKET_AWS_ACCESS_KEY_ID="access-key"
    local BUCKET_BUCKET_AWS_SECRET_ACCESS_KEY="secret-key"
    _clear_aws
    aws_s3_download BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "access-key" "aws_access_key" "Is the download access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "secret-key" "aws_secret_key" "Is the download secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 cp s3://bucket/path/from to" "aws_params" "Is the download command correct for AWS"
}
check_aws_s3_download_command_credentials

function check_aws_s3_download_dir_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_download_dir BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 sync s3://bucket/path/from to --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"
}
check_aws_s3_download_dir_command

# function() instead of function{} so that the stub functions are scoped
function check_aws_s3_download_artifacts_command() (
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PROJECT_DIR="/tmp"
    local CI_PIPELINE_ID="12345"
    local ARTIFACT_DEPENDENCY="job"
    local PREVIOUS_JOB_ID="67890"
    local ARTIFACTS_DIR="/tmp/artifacts"
    local ARTIFACTS_TEMP_SUFFIX="-temp"
    local RC_FILE="/tmp/rc"
    local KCIDB_DUMPFILE_NAME="kcidb_all.json"
    local artifacts_mode="s3"
    local MAX_TRIES=20
    local MAX_WAIT=5

    function aws_s3api_list_objects {
        if [ "$2" = "${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/" ]; then
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/"
        else
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/rc"
            echo "path/${CI_PIPELINE_ID}/${ARTIFACT_DEPENDENCY}/${PREVIOUS_JOB_ID}/artifacts/"
        fi
    }
    _clear_aws
    echo dummy > "${RC_FILE}"
    echo dummy > "${RC_FILE}${ARTIFACTS_TEMP_SUFFIX}"
    echo dummy > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}"
    echo dummy > "${CI_PROJECT_DIR}/${KCIDB_DUMPFILE_NAME}${ARTIFACTS_TEMP_SUFFIX}"
    aws_s3_download_artifacts

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 sync s3://bucket/path/12345/job/67890/artifacts /tmp/artifacts --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"
)
check_aws_s3_download_artifacts_command

function check_aws_s3_upload_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_upload BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the upload access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the upload secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 cp from s3://bucket/path/to" "aws_params" "Is the upload command correct for AWS"
}
check_aws_s3_upload_command

function check_aws_s3_upload_dir_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_upload_dir BUCKET from to

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 sync from s3://bucket/path/to --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"
}
check_aws_s3_upload_dir_command

function check_aws_s3_upload_artifacts_command {
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PROJECT_DIR="/tmp"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    local ARTIFACTS_DIR="/tmp/artifacts"
    local RC_FILE="/tmp/rc"
    local ARTIFACTS_TEMP_SUFFIX="-temp"
    local KCIDB_DUMPFILE_NAME="kcidb_all.json"
    local artifacts_mode="s3"
    local MAX_TRIES=20
    local MAX_WAIT=5
    _clear_aws
    mkdir -p "${ARTIFACTS_DIR}"
    aws_s3_upload_artifacts

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the sync access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the sync secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params.3)" "--endpoint-url https://endpoint.url s3 sync /tmp/artifacts s3://bucket/path/12345/job/67890/artifacts --size-only --only-show-errors" "aws_params" "Is the sync command correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 cp index.html s3://bucket/path/12345/job/67890/index.html" "aws_params" "Is the index copy command correct for AWS"
}
check_aws_s3_upload_artifacts_command

function check_aws_s3_ls_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_ls BUCKET dir

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 ls s3://bucket/path/dir" "aws_params" "Is the ls command correct for AWS"
}
check_aws_s3_ls_command

function check_aws_s3api_list_objects_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3api_list_objects BUCKET dir param1 param2

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3api list-objects --bucket bucket --prefix path/dir param1 param2" "aws_params" "Is the list-objects command correct for AWS"
}
check_aws_s3api_list_objects_command

function check_aws_s3_rm_command {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    _clear_aws
    aws_s3_rm BUCKET dir --recursive

    _check_leaked_vars
    _check_equal "$(cat /tmp/aws_access_key)" "" "aws_access_key" "Is the ls access key correct for AWS"
    _check_equal "$(cat /tmp/aws_secret_key)" "" "aws_secret_key" "Is the ls secret key correct for AWS"
    _check_equal "$(cat /tmp/aws_params)" "--endpoint-url https://endpoint.url s3 rm s3://bucket/path/dir --recursive" "aws_params" "Is the rm command correct for AWS"
}
check_aws_s3_rm_command

function check_aws_s3_url_command {
    local BUCKET="https://endpoint.url/bucket/path"
    aws_url="$(aws_s3_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://endpoint.url/bucket/path/dir" "aws_url" "Is the url correct for AWS"
}
check_aws_s3_url_command

function check_aws_s3_url_command_proxy {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    aws_url="$(aws_s3_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://proxy.url/bucket/path/dir" "aws_url" "Is the url correct for AWS"
}
check_aws_s3_url_command_proxy

function check_aws_s3_browse_url_command {
    local BUCKET="https://endpoint.url/bucket/path"
    aws_url="$(aws_s3_browse_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://endpoint.url/bucket/index.html?prefix=path/dir" "aws_url" "Is the browse url correct for AWS"
}
check_aws_s3_browse_url_command

function check_aws_s3_browse_url_command_proxy {
    local BUCKET="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    aws_url="$(aws_s3_browse_url BUCKET dir)"

    _check_leaked_vars
    _check_equal "${aws_url}" "https://proxy.url/bucket/index.html?prefix=path/dir" "aws_url" "Is the browse url correct for AWS"
}
check_aws_s3_browse_url_command_proxy

function check_artifact_url_gitlab {
    local artifacts_mode="gitlab"
    local CI_API_JOB_URL="CI_API_V4_URL/projects/CI_PROJECT_ID/jobs/CI_JOB_ID"
    url="$(artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "${CI_API_JOB_URL}/artifacts/artifacts/file" "url" "Is the artifact url correct when artifacts_mode=gitlab"
}
check_artifact_url_gitlab

function check_artifact_url_s3 {
    local artifacts_mode="s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://endpoint.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the artifact url correct when artifacts_mode=s3"
}
check_artifact_url_s3

function check_artifact_url_s3_proxy {
    local artifacts_mode="s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://proxy.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the artifact url correct when artifacts_mode=s3"
}
check_artifact_url_s3_proxy

function check_browsable_artifact_url_gitlab {
    local artifacts_mode="gitlab"
    local CI_JOB_URL="GITLAB_URL/PROJECT_NAME/-/jobs/CI_JOB_ID"
    url="$(browsable_artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "${CI_JOB_URL}/artifacts/file/artifacts/file" "url" "Is the browsable artifact url correct when artifacts_mode=gitlab"
}
check_browsable_artifact_url_gitlab

function check_browsable_artifact_url_s3 {
    local artifacts_mode="s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://endpoint.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the browsable artifact url correct when artifacts_mode=s3"
}
check_browsable_artifact_url_s3

function check_browsable_artifact_url_s3_proxy {
    local artifacts_mode="s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_url "artifacts/file")"

    _check_leaked_vars
    _check_equal "${url}" "https://proxy.url/bucket/path/12345/job/67890/artifacts/file" "url" "Is the browsable artifact url correct when artifacts_mode=s3"
}
check_browsable_artifact_url_s3_proxy

function check_browsable_artifact_directory_url_gitlab {
    local artifacts_mode="gitlab"
    local CI_JOB_URL="GITLAB_URL/PROJECT_NAME/-/jobs/CI_JOB_ID"
    url="$(browsable_artifact_directory_url "artifacts/dir")"

    _check_leaked_vars
    _check_equal "${url}" "${CI_JOB_URL}/artifacts/browse/artifacts/dir" "url" "Is the browsable artifact directory url correct when artifacts_mode=gitlab"
}
check_browsable_artifact_directory_url_gitlab

function check_browsable_artifact_directory_url_s3 {
    local artifacts_mode="s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_directory_url "artifacts/dir")"

    _check_leaked_vars
    _check_equal "${url}" "https://endpoint.url/bucket/index.html?prefix=path/12345/job/67890/artifacts/dir" "url" "Is the browsable artifact directory url correct when artifacts_mode=s3"
}
check_browsable_artifact_directory_url_s3

function check_browsable_artifact_directory_url_s3_proxy {
    local artifacts_mode="s3"
    local BUCKET_ARTIFACTS="https://endpoint.url/bucket/path"
    local BUCKET_BUCKET_PROXY="https://proxy.url"
    local CI_PIPELINE_ID="12345"
    local CI_JOB_NAME="job"
    local CI_JOB_ID="67890"
    local AWS_TARGET_PATH="${CI_PIPELINE_ID}/${CI_JOB_NAME}/${CI_JOB_ID}"
    url="$(browsable_artifact_directory_url "artifacts/dir")"

    _check_leaked_vars
    _check_equal "${url}" "https://proxy.url/bucket/index.html?prefix=path/12345/job/67890/artifacts/dir" "url" "Is the browsable artifact directory url correct when artifacts_mode=s3"
}
check_browsable_artifact_directory_url_s3_proxy

_failed_check
