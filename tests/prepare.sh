#!/bin/bash
set -euo pipefail

# shellcheck source-path=SCRIPTDIR/..
source tests/source_functions.sh
# shellcheck source-path=SCRIPTDIR/..
source tests/helpers.sh

# Add some example projects to test.
export GITLAB_COM_PACKAGES="cki-lib datadefinition kpet https://gitlab.com/cki-project/upt"
export DATA_PROJECTS="https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db"
export cki_lib_pip_url="git+https://gitlab.com/cki-project/cki-lib.git@main"
export kpet_db_pip_url="git+https://gitlab.com/redhat/centos-stream/tests/kernel/kpet-db.git@main"
# Additional variables normally supplied via the pipeline.
export SOFTWARE_DIR=${CI_PROJECT_DIR}/software
export SOFTWARE_ARTIFACTS_PATH=artifacts/software.tar.gz
export VENV_DIR=${SOFTWARE_DIR}/venv
export VENV_PY3=${VENV_DIR}/bin/python3
export ARTIFACT_DEPENDENCY=${CI_JOB_NAME}

# Verify all software is installed in ${SOFTWARE_DIR}
function verify_installed {
  cd "${SOFTWARE_DIR}"
    echo_green "Verifying directories are present."
    test -d venv

    echo_green "Verifying Python packages."
    for PROJECT in ${GITLAB_COM_PACKAGES}; do
        if [[ "${PROJECT}" =~ ^http ]]; then
            PROJECT="$(basename "${PROJECT}")"
        fi
        ${VENV_PY3} -m pip show "${PROJECT}"
    done
    for PROJECT in ${DATA_PROJECTS}; do
        test -d "$(basename "${PROJECT}")"
    done
  cd "${OLDPWD}"

  echo_green "🥳🤩 SUCCESS!"
}

eval "$(tests/setup-minio.sh)"

setup_software
verify_installed
aws_s3_upload_artifacts
rm -rf "${SOFTWARE_DIR}" "${CI_PROJECT_DIR:?}/${SOFTWARE_ARTIFACTS_PATH}"

aws_s3_download_artifacts
extract_software
verify_installed

rm -rf wheel-verify-venv "${SOFTWARE_DIR}" "${ARTIFACTS_DIR}"
