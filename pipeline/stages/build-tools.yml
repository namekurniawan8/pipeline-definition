---
# gitlab-yaml-shellcheck: main=../../cki_pipeline.yml

.compile_tools: |
  # Natively compile the kernel tools.
  # These are native builds, drop the cross compile from per-arch vars!
  unset CROSS_COMPILE
  # obtain data from build job
  build_job_compiler="$(kcidb_build get compiler)"
  build_job_make_opts="$(kcidb_build get command)"
  build_job_build_time="$(kcidb_build get duration)"
  # Allow easy handling of failed jobs by including all data.
  rc_state_set compiler "${build_job_compiler} / $("${compiler}" --version | head -1)"
  kcidb_build set compiler "${build_job_compiler} / $("${compiler}" --version | head -1)"

  # Extract vmlinux.h from the kernel-devel, if available. This is needed to compile
  # bpftool and selftests.
  kernel_version="$(kcidb_checkout get misc/kernel_version)"
  vmlinux_path="/usr/src/kernels/${kernel_version}.${ARCH_CONFIG}"
  cd /
    # Run the cpio command from the root directory so it's placed into the right spot right away
    # because we can't get rid of the leading "./"
    rpm2cpio "${ARTIFACTS_DIR}/${package_name}-devel-${kernel_version}.${ARCH_CONFIG}.rpm" \
             | cpio -idmv ".${vmlinux_path}/vmlinux.h"
  cd "${CI_PROJECT_DIR}"

  RPMBUILD_WITHOUT_NATIVE_TOOLS_array=()
  create_array_from_string RPMBUILD_WITHOUT_NATIVE_TOOLS
  # Disable selftests and bpftool until https://bugzilla.redhat.com/show_bug.cgi?id=2054579
  # is backported to 9.0
  # MR for 8.4 is pending: https://gitlab.com/redhat/rhel/src/kernel/rhel-8/-/merge_requests/2535
  if [[ "${branch}" == '9.0' ]] || [[ "${branch}" == '8.4' ]] ; then
    RPMBUILD_WITHOUT_NATIVE_TOOLS_array+=(bpftool selftests)
  fi

  build_command=(rpmbuild --rebuild)
  for elt in "${RPMBUILD_WITHOUT_NATIVE_TOOLS_array[@]+"${RPMBUILD_WITHOUT_NATIVE_TOOLS_array[@]}"}"; do
      build_command+=(--without "${elt}")
  done

  if [[ "${compiler}" == 'clang' ]] ; then
    build_command+=(--with toolchain_clang)
  fi

  kcidb_build set command "${build_job_make_opts} / ${build_command[*]}"
  rc_state_set make_opts "${build_job_make_opts} / ${build_command[*]}"
  echo_yellow "Compiling the kernel tools with: ${build_command[*]}"

  # Update the log URL to contain tools build logs too
  kcidb_build set log_url "$(artifact_url "${BUILDLOG_PATH}")"

  rc_state_set stage_build fail
  store_time
  "${build_command[@]}" artifacts/*.src.rpm 2>&1 | ts -s | tee -a "${CI_PROJECT_DIR}/${BUILDLOG_PATH}" | tail -n 25
  rc_state_set stage_build pass
  rc_state_set build_time "$((build_job_build_time + $(calculate_duration)))"
  kcidb_build set-int duration "$((build_job_build_time + $(calculate_duration)))"

  # Move over needed artifacts. Rename RC file to avoid conflicts with the main build job.
  mv "${WORKDIR}/rpms/"*/*.rpm "${ARTIFACTS_DIR}/"
  echo_green "Kernel tools compiled successfully. Check the publish stage for full yum/dnf repository."

.build-tools:
  extends: [.with_artifacts, .with_retries, .with_builder_image]
  stage: build-tools
  timeout: 5h
  variables:
    # the publish stage expects exactly the same artifacts, with or without native tools
    ARTIFACTS: !reference [.build, variables, ARTIFACTS]
  before_script:
    - !reference [.common-before-script]
  script:
    - |
    - !reference [.build-prepare-rpm]
    - !reference [.compile_tools]
    - aws_s3_upload_artifacts
  after_script:
    - !reference [.common-after-script-head]
    - |
      if [[ "${CI_JOB_STATUS}" == "failed" ]]; then
        kcidb_build set-bool valid false
        aws_s3_upload_artifacts
      fi
    - !reference [.common-after-script-tail]
  rules:
    - !reference [.skip_without_native_tools]
    - !reference [.skip_without_stage]
    - !reference [.skip_without_source]
    - !reference [.skip_without_architecture]
    - when: on_success

build_tools ppc64le:
  extends: [.build-tools, .ppc64le_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder ppc64le
      build ppc64le
  needs:
    - prepare builder ppc64le
    - build ppc64le
  tags:
    - pipeline-build-runner-ppc64le${POWER_BUILDER_SUFFIX}

build_tools aarch64:
  extends: [.build-tools, .aarch64_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder aarch64
      build aarch64
  needs:
    - prepare builder aarch64
    - build aarch64
  tags:
    - pipeline-build-runner-aarch64

build_tools s390x:
  extends: [.build-tools, .s390x_variables]
  variables:
    ARTIFACT_DEPENDENCY: |-
      prepare builder s390x
      build s390x
  needs:
    - prepare builder s390x
    - build s390x
  tags:
    - pipeline-build-runner-s390x

# build_tools missing on purpose for ppc64 as it's not supported for RHEL7
# which is the only release supporting ppc64
